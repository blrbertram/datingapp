Angular v5 Snippets by John Papa
Angular Files  by Alexander Ivanichev
Angular Language Services from Angular
Bracket Pair Colorizer   by CoenraadS
Material Icon Theme  by Philipp Kief
Path Intellisense by Christian Kohler
Prettier - Code Formatter  by Esben Petersen
tslint from egamma
nuget package manager from jmrog
Typescript Hero by Christoph Buhler
Debugger for Chrome by Microsoft

